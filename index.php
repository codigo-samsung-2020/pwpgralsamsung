<?php 
include 'checkip.php';
$ch = new CheckIP();
    //Example ip for localhost 
    //$user_ip = "1.1.1.199";
$user_ip = $ch->getUserIP();
$inicial = $user_ip[0];

function getPhone(){ 
  require_once 'controller/conn/connection.php';
  $connect = new connection();
  $connection=$connect->connections();
  $sql = "SELECT * FROM contacto WHERE id_contacto = 1";
  $result = mysqli_query($connection, $sql);
  $tabla = "";
  while($row = mysqli_fetch_array($result)){
    echo''.$row['telefono'].'';    
  }
}

$ch->validateIP($inicial, "serviciosamsung-centroautorizadomexico", $user_ip);
?>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="Servicios Integrales para el Hogar CDMX">
  <meta name="description" content="Servicio aLavadoras en Ciudad de México. Servicio Urgente a Domicilio en 2hrs. Centro de Servicio Especialista. Reparación, Mantenimiento e Instalación de Refrigeradores, Estufas, Lavadoras, Centros de Lavado, Hornos. Teléfono: <?php getPhone();?>">
  <meta name="Keywords" content="servicio samsung, servicio tecnico samsung, servicio de mantenimiento samsung, servicio de reparacion de refrigeradores samsung, servicio samsung en cdmx, servicio samsung a domicilio, servicio samsung urgente, samsung servicio, reparacion de lavadoras, servicio para lavadoras, servicio para lavadoras a domicilio, servicio a domicilio para estufas, reparamos estufas a domicilio, tecnicos de lavadoras, tecnicos de lavadoras a domicilio, servicio para lavadoras, servicio a lavadoras, reparacion de lavadoras, reparamos lavadoras, servicio a lavadoras a domicilio, servicio para lavadoras en cdmx, service samsung">
  <meta name="title" content="Servicio Samsung en CDMX | Técnicos Especialistas | -10% de Descuento">
  <meta name="theme-color" content="#000">
  <meta name="Revisit-after" content="7 days">
  <meta name="Robots" content="all, follow">
  <meta name=”distribution” content=”global”/>
  <meta http-equiv="Cache-Control" content="max-age=84600">
  <title>Servicio Samsung en CDMX | Técnicos Especialistas | -10% de Descuento | Contacto  <?php getPhone();?></title>

  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <!-- Custom styles for this template -->
  <link href="assets/css/our-template.css" rel="stylesheet">
  <script src="https://kit.fontawesome.com/7533bfdde7.js" crossorigin="anonymous"></script>

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-167643210-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-167643210-1');
  </script>

  <!-- Google Analytics Events-->
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-167643210-1', 'auto');
    ga('send', 'pageview');
  </script>
  <!-- End Google Analytics -->

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>
<body>
  <nav class="adjust-space navbar navbar-expand-md navbar-dark bg-black fixed-top">
    <a class="navbar-brand" href="#"><img src="assets/img/logo-samsung.webp" class="logo-config"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <!--<a class="nav-link btn-web-action" href="https://wa.me/525565020417" onclick="ga('send', 'event', 'inicio', 'whatsheader', 'clic')" >Mantenimiento</a>-->
          <a class="nav-link" href="tel:+52<?php getPhone();?>" onclick="ga('send', 'event', 'inicio', 'callheader', 'clic')" >Mantenimiento</a>
        </li>
        <li class="nav-item ">
          <!--<a class="nav-link btn-web-action" href="https://wa.me/525565020417" onclick="ga('send', 'event', 'inicio', 'whatsheader', 'clic')" >Reparación</a>-->
          <a class="nav-link" href="tel:+52<?php getPhone();?>" onclick="ga('send', 'event', 'inicio', 'callheader', 'clic')" >Reparación</a>
        </li>
        <li class="nav-item ">
          <!--<a class="nav-link btn-web-action" href="https://wa.me/525565020417" onclick="ga('send', 'event', 'inicio', 'whatsheader', 'clic')" >Instalación</a>-->
          <a class="nav-link" href="tel:+52<?php getPhone();?>" onclick="ga('send', 'event', 'inicio', 'callheader', 'clic')" >Instalación</a>
        </li>
        
      </ul>
      <div class="form-inline my-2 my-lg-0">

        <!--<a class="btn btn-outline-light my-2 my-sm-0 btn-web-action" type="submit" href="https://wa.me/525565020417" onclick="ga('send', 'event', 'inicio', 'whatsheader', 'clic')">Agendar Visita a Domicilio</a>-->

        <a class="btn btn-outline-light my-2 my-sm-0" type="submit" href="tel:+52<?php getPhone();?>" onclick="ga('send', 'event', 'inicio', 'callheader', 'clic')">Agendar Visita a Domicilio</a>

      </div>
    </div>
  </nav>
  <div class="container-fluid top-init">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img src="assets/img/instalacion-de-refeigeradores.webp" class="bd-placeholder-img">

          <div class="container">
            <div class="carousel-caption text-left">
              <h1>Servicio Samsung</h1>
              <p>Si tu equipo llega a presentar fallas no dudes en llamarnos, contamos con todo un equipo de trabajo altamente capacitado para solucionar los problemas de tu equipo.</p>
              <p><!--<a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525565020417" onclick="ga('send', 'event', 'inicio', 'whatsslide', 'clic')">Obtén -10% de Descuento</a>--> <a class="btn btn-lg btn-primary " href="tel:+52<?php getPhone();?>" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')">Obtén -10% de Descuento</a></p>
            </div>
          </div>
        </div>
        <div class="carousel-item">
          <img src="assets/img/mantenimiento-de-estufas.webp" class="bd-placeholder-img">
          <div class="container">
            <div class="carousel-caption">
              <h1>Servicio de Reparación</h1>
              <p>Brindamos un servicio de excelencia con los más altos estándares de calidad, contamos con refacciones originales y garantía por escrito.</p>
              <p><!--<a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525565020417" onclick="ga('send', 'event', 'inicio', 'whatsslide', 'clic')">Servicio a Domicilio</a>--> <a class="btn btn-lg btn-primary" href="tel:+52<?php getPhone();?>" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')">Servicio a domicilio</a></p>
            </div>
          </div>
        </div>
        <div class="carousel-item">
          <img src="assets/img/samsunglav-slide.webp" class="bd-placeholder-img">
          <div class="container">
            <div class="carousel-caption text-right">
              <h1>Servicio de Mantenimiento</h1>
              <p>Somos líderes en servicios técnicos de mantenimiento de línea blanca de la marca SAMSUNG en la CDMX y Área Metropolitana.</p>
              <p><!--<a class="btn btn-lg btn-primary btn-web-action" href="https://wa.me/525565020417" onclick="ga('send', 'event', 'inicio', 'whatsslide', 'clic')">Reservar Visita</a>--> <a class="btn btn-lg btn-primary" href="tel:+52<?php getPhone();?>" onclick="ga('send', 'event', 'inicio', 'callslide', 'clic')"> Visita a Domicilio</a></p>
            </div>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>

  </div>

  <div class="container-fluid bg-primary text-center text-white inside-block">
    <div class="row">
      <div class="col-md">
        <p>
          <!--<a href="https://wa.me/52<?php getPhone();?>" class="text-service btn-web-action" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')"><b>Servicio Urgente </b></a>-->
          <a href="tel:+52<?php getPhone();?>" class="text-service" onclick="ga('send', 'event', 'inicio', 'callbody', 'clic')"><b>Servicio Urgente </b></a><i class="fas fa-shipping-fast"></i>
        </p>

      </div>
      <div class="col-md">
        <p>
          <!--<a href="https://wa.me/52<?php getPhone();?>"  class="text-service btn-web-action " onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')"><b>Agendar Visita </b></a> -->
          <a href="tel:+52<?php getPhone();?>" class="text-service" onclick="ga('send', 'event', 'inicio', 'callbody', 'clic')"><b>Agendar Visita </b></a><i class="far fa-calendar-check"></i>
        </p>
      </div>
      <div class="col-md">
        <p>
          <!--<a href="tel:+52<?php getPhone();?>" class="text-service btn-web-action" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')"><b>Enviar Whatsapp </b></a>-->
          <a href="tel:+52<?php getPhone();?>" class="text-service" onclick="ga('send', 'event', 'inicio', 'callbody', 'clic')"><b>Enviar Whatsapp </b></a><i class="fab fa-whatsapp"></i>
        </p>
      </div>
    </div>
  </div>

  <div class="container-fluid text-white">

    <div class="row">
      <div class="col-md-6 bg-black grid-lead ">
        <h2 class="text-title"><b>Reparación</b></h2>
        <p class="text-lead">Reparamos toda la linea de refrigeradores Famili Hub&trade;, French Door, Side by Side, Bottom Mount. Lavadoras carga superior, Lavadoras carga frontal, Lavasecadoras y Secadoras. Estufas tradicionales y electrónicas.</p>
        <p>
          <!--<a href="https://wa.me/52<?php getPhone();?>" class="btn btn-success my-2 shadow-lg btn-lg btn-web-action" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')">Obtén -10% de Desc</a>-->
          <a href="tel:+52<?php getPhone();?>" class="btn btn-primary my-2 shadow-lg btn-lg" onclick="ga('send', 'event', 'inicio', 'callbody', 'clic')">Obtén -10% de Desc</a>

        </p>
      </div>
      <div class="col-md-6 text-center grid-lead-img"><img class="img-rep grid-img-lead estufa" src="assets/img/estufa-samsung.webp"></div>

      <div class="col-md-6 text-center grid-lead-img hide-tab-mob" >
        <img class="img-rep grid-img-lead refri" src="assets/img/reparacion-de-refrigerador-samsung.webp">
      </div>
      <div class="col-md-6 bg-primary grid-lead">
        <h2 class="text-title"><b>Instalación</b></h2>
        <p class="text-lead">Hacemos instalaciones a domicilio de Estufas, Refrigeradores, Lavadoras, Lavasecadoras, Secadoras y Centros de Lavado. Contamos con obertura en toda la Ciudad de México y Área Metropolitana.</p>
        <!--<a href="https://wa.me/52<?php getPhone();?>" class="btn btn-success my-2 shadow-lg btn-lg btn-web-action" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')">Enviar Whatsapp</a>-->
        <a href="tel:+52<?php getPhone();?>" class="btn btn-success my-2 shadow-lg btn-lg" onclick="ga('send', 'event', 'inicio', 'callbody', 'clic')">Agendar Visita Técnica</a>
      </div>
      <div class="col-md-6 text-center grid-lead-img show-tab-mob">
        <img class="img-rep grid-img-lead refri" src="assets/img/reparacion-de-refrigerador-samsung.webp">
      </div>
      <div class="col-md-6 bg-black grid-lead">
        <h2 class="text-title"><b>Mantenimiento</b></h2>
        <p class="text-lead"> Si tienes algún problema con tu estufa, refrigerador, congelador, lavadora, secadora, lavasecadora o centro de lavado no dudes en comunicarte con nosotros, somos expertos y contamos con refacciones originales Samsung.</p>
        <!--<a href="https://wa.me/52<?php getPhone();?>" class="btn btn-success my-2 shadow-lg btn-lg btn-web-action" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')">Agendar vía Whatsapp</a>-->
        <a href="tel:+52<?php getPhone();?>" class="btn btn-primary my-2 shadow-lg btn-lg" onclick="ga('send', 'event', 'inicio', 'callbody', 'clic')">Servicio Urgente en 2hrs</a>
      </div>
      <div class="col-md-6 text-center grid-lead-img" id="img-mob">
        <img class="img-rep grid-img-lead lavadora" src="assets/img/estufa-servicio-frigidaire.webp">
      </div>
    </div>

  </div>

<div class="container-fluid bg-primary text-center inside-block">
  <div class="row">
    <div class="col-md-12 text-white">
      <h2><b>Métodos de Pago</b></h2>
      <p class="text-lead">Contamos con diferentes métodos de pago: <b>Tarjetas de crédito, débito, transferencia bancaria, pago a distancia, mercado pago y efectivo.</b></p>
      <br>
    </div>
    <div class="col-md-4 pay">
      <img src="assets/img/visa.webp" class=" visa" alt="reparacion de lavadoras" width="105px">
    </div>
    <div class="col-md-4 pay">
      <img src="assets/img/mercadopago.webp" class=" clip" alt="reparacion de lavadoras" width="80px">
    </div>
    <div class="col-md-4 pay">
      <img src="assets/img/mastercard.webp" class="master" alt="reparacion de lavadoras" width="70px">
    </div>
    <!--
    <div class="col-md-3 pay">
      <img src="assets/img/AMEX.webp" class=" bbva" alt="reparacion de lavadoras" width="120px">
    </div>-->
  </div>
</div>



<div class="container-fluid bg-pharallax text-center">
  <p class="text-white text-parallax"><b>Agenda una Visita</b></p>
  <a href="tel:+52<?php getPhone();?>" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')" class="btn btn-lg btn-primary btn-left-parallax">Solicitar Servicio</a>
  <a href="tel:+52<?php getPhone();?>" onclick="ga('send', 'event', 'inicio', 'whatsbody', 'clic')" class="btn btn-lg btn-warning btn-format btn-right-parallax" >Llamar Ahora</a>
</div>

<div class="container-fluid bg-black text-center padd text-white">
  <div class="row">
    <div class="col-md-8">
      <p class="cta-txt text-center"><b>¿Necesitas Ayuda? ¡Llama Ahora!</b></p>
    </div>
    <div class="col-md-4 text-center">
      <div class="">
        <!--<a href="tel:+525621855180" class="btn btn-primary shadow-lg btn-lg text-white margin btn-web-action" onclick="ga('send', 'event', 'ge-sub', 'enviarwhats', 'clic')">Llama al <?php getPhone();?></a>-->

        <a href="tel:+52<?php getPhone();?>" class="btn btn-primary mt shadow-lg btn-lg text-white margin " onclick="ga('send', 'event', 'ge-sub', 'enviarwhats', 'clic')">Llama al <?php getPhone();?></a>
      </div>
      <!--<a href="#" class="btn btn-primary my-2 shadow-lg btn-lg">Solicitar Cotización</a>-->
    </div>
  </div>
</div>
  <!--
  <div class="container-fluid text-center inside-block">
    <div class="row">
      <div class="col-md-12">
        <h2>Métodos de Pago</h2>
        <br>
      </div>
      <div class="col-md-3 pay">
        <img src="assets/img/clip.webp" class=" clip" alt="reparacion de lavadoras" width="60px">
      </div>
      <div class="col-md-3 pay">
        <img src="assets/img/bbva.webp" class=" bbva" alt="reparacion de lavadoras" width="120px">
      </div>
      <div class="col-md-3 pay">
        <img src="assets/img/visa.webp" class=" visa" alt="reparacion de lavadoras" width="110px">
      </div> 
      <div class="col-md-3 pay">
        <img src="assets/img/mastercard.webp" class="master" alt="reparacion de lavadoras" width="70px">
      </div>
    </div>
  </div>
-->

<!-- START ALERT -->
<div class="toast sticky bg-primary fade hide" data-autohide="false">
  <div class="toast-header bg-blue">
    <strong class="mr-auto color-electrolux">¡Gran Oferta!</strong>
    <small>Servicio en 2 hrs</small>
    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast">×</button>
  </div>
  <div class="toast-body bg-blue-ge text-white">
   <b><span class="">Aprovecha un -10% de descuento en tu primer servicio.</span> Contamos con las medidas de seguridad necesarias contra covid-19. Aprovecha y da clic en <!--<a href="tel:+52<?php getPhone();?>" onclick="ga('send', 'event', 'ge-sub', 'whatsbody', 'clic')" class="text-dark btn-web-action">  agendar una visita técnica</a>-->
    <a href="tel:+52<?php getPhone();?>" onclick="ga('send', 'event', 'ge-sub', 'llamada', 'clic')" class="text-dark "> agendar una visita técnica</a></b>
  </div>
</div>


<footer class="container py-5">
  <div class="row">
    <div class="col-6 col-md">
      <h5><b>Sucursales</b></h5>
      <ul class="list-unstyled text-small">
        <li>Cuauhtémoc</li>
        <li>Miguel Hidaldo</li>
        <li>Coyoacán</li>
        <li>Tlalpan</li>
        <li>Iztapalapa</li>
        <li>Iztacalco</li>
        <li>Azcapotzalco</li>
      </ul>
    </div>
    <div class="col-6 col-md">
      <ul class="list-unstyled text-small">
        <h5><b>Servicio Urgente</b></h5>
        <li>Milpa Alta</li>
        <li>Tláhuac</li>
        <li>Xochimilco</li>
        <li>Venustiano Carranza</li>
        <li>Cuajimalpa</li>
        <li>Gustavo A. Madero</li>
        <li>Benito Juárez</li>
      </ul>
    </div>
    <div class="col-6 col-md">
      <h5><b>Contacto</b></h5>
      <ul class="list-unstyled text-small">
        <li>
          <!--<a class="text-muted btn-web-action" href="https://wa.me/52<?php getPhone();?>" onclick="ga('send', 'event', 'inicio', 'footwhats', 'clic')"><b class="text-primary">Agendar Visita</b></a>-->
          <a class="text-muted btn-mobile-action" href="tel:+52<?php getPhone();?>" onclick="ga('send', 'event', 'inicio', 'footcall', 'clic')"><b class="text-primary">Agendar Visita</b></a>
        </li>
        <li><a class="text-success " href="tel:+52<?php getPhone();?>" onclick="ga('send', 'event', 'inicio', 'footwhats', 'clic')"><b>Envia un Whatsapp</b></a></li>
        <li><!--<a class="btn btn-warning btn-web-action" href="https://wa.me/52<?php getPhone();?>" onclick="ga('send', 'event', 'inicio', 'footwhats', 'clic')"><b>Obtén -10% de Desc</b></a>--><a class="btn-mobile-action text-danger" href="tel:+52<?php getPhone();?>" onclick="ga('send', 'event', 'inicio', 'footwhats', 'clic')"><b>Obtén -10% desc</b></a></li>
      </ul>
    </div>
    <div class="col-12 col-md">
      <img src="assets/img/ssl_comodo.webp" class="comodo">
      <small class="d-block mb-3 text-muted">Todas las imágenes y logotipos son propiedad de la marca correspondiente.</small>
    </div>
  </div>
</footer>
<script type="text/javascript" src="assets/js/samsung.js"></script>

</html>
